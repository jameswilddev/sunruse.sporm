# Stored Procedure Object-Relational Mapping #
An ORM for calling stored procedures in Microsoft SQL Server 2012+ using a mockable C# interface; each stored procedure is brought into C# as a single method call with generated models for result sets.  This could be used to make a very easy to unit test against database access layer as all SQL is encapsulated in the database and the DAL can be mocked by simply mocking out the methods in the DAL object.

At present this is in early stages and has not been heavily tested.

## Current known limitations ##
* Error handling when generating code is poor.
* Many SQL types are currently unsupported.
* Only Microsoft SQL Server 2012+ is supported for generating a DAL from, but that DAL may be used with other database engines.  (untested)
* Only one result set may be returned by a stored procedure.
* Table valued parameters are unsupported, but are planned for the future.